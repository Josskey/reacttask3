import "./style.css";
import { useEffect, useLayoutEffect, useRef, useState } from "react";


export default function App() {
  return (
    <div className="App">
      <ResetTime />
    </div>
  );
}
function TimerStop() {
  const [enabled, setEnabled] = useState(false);
  const [timer, setTimer] = useState(0);
  const [countDown, setCountDown] = useState(10);

  const toggleTimer = () => {
    setEnabled((e) => !e);
    setTimer(0);
    setCountDown(10);
  };

  useEffect(() => {
    if (!enabled) return;
    const i = setInterval(() => {
      setTimer((t) => t + 1);
      setCountDown((c) => Math.max(c - 1, 0));
    }, 100);
    return () => {
      clearInterval(i);
    };
  }, [enabled]);

  useEffect(() => {
    if (countDown === 0) {
      alert("Count down");
    }
  }, [countDown]);

  return (
    <div>
      <button onClick={toggleTimer}>{!enabled ? "Start" : "Stop"}</button>
      <div>timer {timer}</div>
      <div>countDown {countDown}</div>
    </div>
  );
}

function TimerPause() {
  const [enabled, setEnabled] = useState(false);
  const [timer, setTimer] = useState(0);
  const [countDown, setCountDown] = useState(10);

  const toggleTimer = () => {
    setEnabled((e) => !e);
  };

  useEffect(() => {
    if (!enabled) return;
    const i = setInterval(() => {
      setTimer((t) => t + 1);
      setCountDown((c) => Math.max(c - 1, 0));
    }, 1000);
    return () => {
      clearInterval(i);
    };
  }, [enabled]);

  useEffect(() => {
    if (countDown === 0) {
      alert("Count down");
    }
  }, [countDown]);

  return (
    <div>
      <button onClick={toggleTimer}>{!enabled ? "Start" : "Pause"}</button>
      <div>timer {timer}</div>
      <div>countDown {countDown}</div>
    </div>
  );
}

function TimerUseNowStop() {
  const [startAt, setStartAt] = useState();

  const now = useNow(100, startAt);

  const fromStart = now - (startAt ?? now);

  const countDown = Math.max(0, 60000 - fromStart);

  const toggleTimer = () => {
    if (startAt) {
      setStartAt();
    } else {
      setStartAt(Date.now());
    }
  };

  const isCountEnd = countDown === 0;

  useEffect(() => {
    if (isCountEnd) {
      alert("Count down");
    }
  }, [isCountEnd]);

  return (
    <div>
      <button onClick={toggleTimer}>{!startAt ? "Start" : "Stop"}</button>
      <div>timer {Math.floor(fromStart / 1000)}</div>
      <div>countDown {Math.ceil(countDown / 1000)}</div>

      <div>timer ms {fromStart}</div>
      <div>countDown ms {countDown}</div>
    </div>
  );
}

function TimerUseNowPause() {
  const [startAt, setStartAt] = useState();
  const [initialTimer, setInitialTimer] = useState(0);

  const now = useNow(100, startAt);

  const timeFromStart = now - (startAt ?? now);

  const timer = timeFromStart + initialTimer;
  const countDown = 60000 - timer;

  const toggleTimer = () => {
    if (startAt) {
      setInitialTimer(timer);
      setStartAt();
    } else {
      setStartAt(Date.now());
    }
  };

  const isCountEnd = countDown === 0;
  useEffect(() => {
    if (isCountEnd) {
      alert("Count down");
    }
  }, [isCountEnd]);

  return (
    <div>
      <button onClick={toggleTimer}>{!startAt ? "Start" : "Pause"}</button>
      <div>timer {Math.floor(timer / 1000)}</div>
      <div>countDown {Math.ceil(countDown / 1000)}</div>
    </div>
  );
}

const resendTimeout = 10000;

function ResetTime() {
  const [sentAt, setSentAt] = useState();

  const now = useNow(1000, sentAt, (now) => {
    if (sentAt && resendTimeout - (now - sentAt) < 0) {
      setSentAt();
    }
  });

  const msToResend = sentAt ? resendTimeout - (now - sentAt) : 0;
  const isDisabled = msToResend > 0;

  const handleSend = () => {
    setSentAt(Date.now());
  };

  return (
    <button disabled={isDisabled} onClick={handleSend}>
      {isDisabled ? `wait: ${Math.floor(msToResend / 1000)}` : "Reset Timer"}
    </button>
  );
}

/*, (now) => {
    if (sentAt && resendTimeout - (now - sentAt) < 0) {
      setSentAt();
    }
  }*/

function useNow(updateInterval, enabled, cb) {
  const cbRef = useRef(cb);
  cbRef.current = cb;
  const [now, setNow] = useState(Date.now());

  useLayoutEffect(() => {
    if (!enabled) {
      return;
    }

    setNow(Date.now());
    cbRef.current?.(Date.now());

    const interval = setInterval(() => {
      setNow(Date.now());
      cbRef.current?.(Date.now());
    }, updateInterval);

    return () => {
      clearInterval(interval);
    };
  }, [updateInterval, enabled]);
  return now;
}
