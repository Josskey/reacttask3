import TodoList from './Component/TodoList';
import Timer from './Component/Timer';
import WindowsSize from './Component/WindowsSize';
function App() {
  return (
    <>
    <WindowsSize/>
    <Timer/>
    <TodoList/>
    </>
  );
}

export default App;
